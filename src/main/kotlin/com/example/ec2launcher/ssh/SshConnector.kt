package com.example.ec2launcher.ssh

import org.apache.sshd.client.SshClient
import org.apache.sshd.client.channel.ClientChannelEvent
import org.apache.sshd.client.session.ClientSession
import org.apache.sshd.common.keyprovider.ClassLoadableResourceKeyPairProvider
import org.apache.sshd.common.util.io.NullOutputStream

val classLoader = object {}::class.java.classLoader!!

fun sshConnect(username: String, host: String, identityFileName: String): ClientSession {
    return SshClient.setUpDefaultClient()
        .apply { start() }
        .connect(username, host, 22)
        .verify()
        .session
        .apply {
            setKeyIdentityProvider(ClassLoadableResourceKeyPairProvider(classLoader, identityFileName))
            auth().verify()
        }
}

fun ClientSession.executeCommand(command: String) {
    createExecChannel(command)
        .apply {
            setOut(NullOutputStream()) // sometimes not working without dummy output stream...
            open().verify()
            waitFor(listOf(ClientChannelEvent.CLOSED), 0)
        }
}

//fun main() {
//
//    val publicDns = "ec2-3-68-76-177.eu-central-1.compute.amazonaws.com"
//    val username = "test-elementor"
//    val password = "\\\$P\\\$Bf9UABYAeJW2vCT59ldKiLG0iLQDwv0"// test-elementor
//    val email = "test-elementor@gmail.com"
//
//    val commandReplace = """
//    docker exec -i mysql-container sh -c "exec sed -i -e 's/localhost:8080/$publicDns/g; s/@wordpress_username/$username/g; s/@wordpress_password/$password/g; s/@wordpress_email/$email/g' /var/shopmbt_nobiliaproject.2021-07-04.sql"
//    """
//
//    val commandInitDb = """
//    docker exec -i mysql-container sh -c "exec mysql -uroot -p1234 wordpress < /var/shopmbt_nobiliaproject.2021-07-04.sql"
//    """
//
//    val identityFile = getResourceFile("wordpress-elementor.pem")
//
//    sshConnect("ubuntu", publicDns, identityFile)
//        .executeCommand(commandReplace + commandInitDb)
//
//}

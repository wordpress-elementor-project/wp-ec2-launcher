package com.example.ec2launcher.config

import mu.KotlinLogging
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.retry.RetryCallback
import org.springframework.retry.RetryContext
import org.springframework.retry.listener.RetryListenerSupport
import org.springframework.retry.support.RetryTemplate
import org.springframework.web.client.RestClientException

@Configuration
class RetryTemplateConfig {

    @Bean
    fun wordpressAccessRetryTemplate(): RetryTemplate {
        return RetryTemplate.builder()
            .maxAttempts(15)
            .fixedBackoff(2000)
            .retryOn(RestClientException::class.java)
            .withListener(LoggingRetryListener())
            .build()
    }

}

private class LoggingRetryListener : RetryListenerSupport() {

    private val log = KotlinLogging.logger { }

    override fun <T : Any?, E : Throwable?> onError(
        context: RetryContext?,
        callback: RetryCallback<T, E>?,
        throwable: Throwable?
    ) {
        log.warn("Exception number {} on get wordpress access: {}", context?.retryCount, throwable.toString())
    }

}

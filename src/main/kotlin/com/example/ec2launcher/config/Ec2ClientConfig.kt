package com.example.ec2launcher.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.ec2.Ec2Client

@Configuration
class Ec2ClientConfig {

    @Bean
    fun ec2Client() = Ec2Client.builder().region(Region.EU_CENTRAL_1).build()

}

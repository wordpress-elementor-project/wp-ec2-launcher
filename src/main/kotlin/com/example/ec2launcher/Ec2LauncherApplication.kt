package com.example.ec2launcher

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Ec2LauncherApplication

fun main(args: Array<String>) {
	runApplication<Ec2LauncherApplication>(*args)
}

package com.example.ec2launcher.dto

data class Credentials(
    val email: String,
    val username: String,
    val password: String
)

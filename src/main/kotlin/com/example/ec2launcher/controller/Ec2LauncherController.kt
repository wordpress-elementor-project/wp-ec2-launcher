package com.example.ec2launcher.controller

import com.example.ec2launcher.dto.Credentials
import com.example.ec2launcher.ssh.executeCommand
import com.example.ec2launcher.ssh.sshConnect
import mu.KotlinLogging
import org.springframework.http.MediaType
import org.springframework.retry.RetryCallback
import org.springframework.retry.support.RetryTemplate
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.getForObject
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter
import software.amazon.awssdk.services.ec2.Ec2Client
import software.amazon.awssdk.services.ec2.model.DescribeInstancesRequest
import software.amazon.awssdk.services.ec2.model.Instance
import software.amazon.awssdk.services.ec2.model.LaunchTemplateSpecification
import software.amazon.awssdk.services.ec2.model.RunInstancesRequest
import java.util.concurrent.CompletableFuture.supplyAsync
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.Executors.newScheduledThreadPool
import java.util.concurrent.TimeUnit

@RestController
class Ec2LauncherController(
    private val restTemplate: RestTemplate,
    private val wordpressAccessRetryTemplate: RetryTemplate,
    private val ec2Client: Ec2Client
) {
    private val log = KotlinLogging.logger { }

    private val availableTemplates = listOf("01", "02")

    private val emitters = ConcurrentHashMap<String, SseEmitter>()

    @PostMapping("instances")
    fun createEc2FromTemplate(@RequestBody credentials: Credentials, @RequestParam template: String): Any {

        if (template !in availableTemplates) throw RuntimeException("Template $template not supported")

        val password = credentials.password.replace("$", "\\\$") // need to screen dollar sign in password
        val (email, username) = credentials

        supplyAsync {
            // launch instance
            val runInstancesRequest = RunInstancesRequest.builder()
                .maxCount(1)
                .minCount(1)
                .launchTemplate(
                    LaunchTemplateSpecification.builder()
                        .launchTemplateName("wordpress-elementor-template-4")
                        .build()
                )
                .build()

            log.info { "Sending request to launch instance..." }
            val response = ec2Client.runInstances(runInstancesRequest)
            log.info { "Instance launching..." }

            val describeInstancesRequest =
                DescribeInstancesRequest.builder().instanceIds(response.instances().first().instanceId()).build()

            val instance = ec2Client.waiter()
                .waitUntilInstanceRunning(describeInstancesRequest).matched()
                .response().get().reservations().first()
                .instances().first()
                .waitWordpressAvailable()

            val publicDnsName = instance.publicDnsName()
            log.info { "Instance launched, publicDnsName: $publicDnsName" }

            log.info { "Instance initializing..." }
            // init instance
            val commandInstallTemplate = """
            docker exec -i wordpress-container sh -c "rm -rf /var/www/html/wp-content/plugins; rm -rf /var/www/html/wp-content/themes;"
            docker exec -i wordpress-container sh -c "cp -rv /var/www/html/wp-content/$template/* /var/www/html/wp-content"
            """

            val commandInitDb = """
            docker exec -i mysql-container sh -c "exec sed -i -e 's/localhost:80/$publicDnsName/g; s/@wordpress_username/$username/g; s/@wordpress_password/$password/g; s/@wordpress_email/$email/g' /var/dumps/$template.sql"
            docker exec -i mysql-container sh -c "exec mysql -uroot -p1234 wordpress < /var/dumps/$template.sql"
            """

            sshConnect("ubuntu", publicDnsName, "wordpress-elementor.pem")
                .executeCommand(commandInstallTemplate + commandInitDb)

            log.info { "Instance up and running!" }

            emitters[username]?.apply {
                send(SseEmitter.event().data(mapOf("url" to "http://$publicDnsName", "type" to "result"), MediaType.APPLICATION_JSON))
                complete()
            }

        }

        return mapOf("result" to "ok")
    }

    private fun Instance.waitWordpressAvailable(): Instance {
        waitWordpressAvailable(publicDnsName())
        return this
    }

    private fun waitWordpressAvailable(publicDnsName: String?): String {
        return wordpressAccessRetryTemplate.execute(RetryCallback {
            restTemplate.getForObject("http://$publicDnsName")
        })
    }

    @GetMapping("{username}/subscribe")
    fun subscribe(@PathVariable username: String): SseEmitter {
        log.info { "subscribed: $username" }

        val scheduledExecutorService = newScheduledThreadPool(1)

        val emitter = emitters.computeIfAbsent(username) {
            SseEmitter(-1).apply {
                onError {
                    scheduledExecutorService.shutdownNow()
                    emitters.remove(username)
                }
                onCompletion {
                    scheduledExecutorService.shutdownNow()
                    emitters.remove(username)
                }
                onTimeout {
                    scheduledExecutorService.shutdownNow()
                    complete()
                    emitters.remove(username)
                }
            }
        }

        scheduledExecutorService.scheduleAtFixedRate(
            { emitter.send(SseEmitter.event().data(mapOf("type" to "ping"), MediaType.APPLICATION_JSON)) },
            0,
            50,
            TimeUnit.SECONDS
        )

        return emitter
    }

}

